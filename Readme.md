# WEKA demo code

This very simple repo contains some use cases for working with the WEKA
 API in your own project.

## Requirements 

Add Weka (>3.5.5) to your build path. In build.gradle it is something like this:

```groovy
// https://mvnrepository.com/artifact/nz.ac.waikato.cms.weka/weka-stable
compile group: 'nz.ac.waikato.cms.weka', name: 'weka-stable', version: '3.8.0'
```

### Clone and open in your favorite IDE

